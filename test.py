import copy
import re


class Config:
    def __init__(self, energy=0, cell_parametrs=[], atom_pos=[], atom_forces=[], total_stress=[], unit = 0):
        self.energy = energy
        self.unit = unit
        self.cell_parametrs = cell_parametrs
        self.total_stress = total_stress
        self.atom_pos = atom_pos
        self.atom_forces = atom_forces


def parse_array_to_float(line):
    res = []
    for x in line.split():
        try:
            res.append(float(x))
        except:
            continue
    return res


def test():
    regex_energy = r"(!\s+total\senergy\s+=\s+)|(\s+Ry)"
    regex_math_energy = r"!\s+total\senergy\s+=\s+"

    regex_unit = r"(new\s+unit-cell\s+volume\s+=\s+.+\(\s+)|(\s+Ang.+)"
    regex_math_unit = r"new\s+unit-cell\s+volume\s+=\s+.+\(\s+"

    regex_atom_count = r"number of atoms/cell\s+=\s+"
    regex_end = r"\s+Writing output data file"

    regex_match_CELL_PARAMETERS = r"^CELL_PARAMETERS\s+\(angstrom\)"
    regex_match_ATOMIC_POSITIONS = r"^ATOMIC_POSITIONS\s+\(angstrom\)"
    regex_match_ATOM_FORCES = r"^Forces acting on atoms \(cartesian axes, Ry\/au\):"
    regex_match_total_stress = r"\s+total\s+stress\s+\(Ry\/bohr\*\*3\)\s+\(kbar\)\s+P=\s+"

    configs = []
    atom_count = 0
    with open('pw_bomd_300K.out', 'r') as alpha:
        lines = alpha.readlines()
        config = Config(0, [], [], [], [])
        count_cell_param = 0
        count_atom_pos = 0
        count_atom_forces = 0
        count_total_stress = 0

        for line in lines:
            if re.search(regex_atom_count, line):
                result = re.sub(regex_atom_count, "", line, 0, re.MULTILINE)
                if result:
                    atom_count = int(result)
            if count_cell_param > 0:
                config.cell_parametrs.append(parse_array_to_float(line))
                count_cell_param = count_cell_param - 1
            if count_total_stress > 0:
                line_arr = parse_array_to_float(line)
                if len(arr) == 0:
                    continue
                config.total_stress.append(line_arr[:3])
                count_total_stress = count_total_stress - 1
            if count_atom_pos > 0:
                config.atom_pos.append(parse_array_to_float(line))
                count_atom_pos = count_atom_pos - 1
            if count_atom_forces > 0:
                arr = parse_array_to_float(line)
                if len(arr) == 0:
                    continue
                config.atom_forces.append(arr[2:])
                count_atom_forces = count_atom_forces - 1
            if re.search(regex_math_unit, line):
                d = float(re.sub(regex_unit, "", line, 0, re.MULTILINE))
                config.unit = d
            if re.search(regex_math_energy, line):
                result = re.sub(regex_energy, "", line, 0, re.MULTILINE)
                if result:
                    config = Config(float(result), [], [], [], [])
            if re.search(regex_match_CELL_PARAMETERS, line.strip()):
                count_cell_param = 3
            if re.search(regex_match_ATOMIC_POSITIONS, line.strip()):
                count_atom_pos = atom_count
            if re.search(regex_match_ATOM_FORCES, line.strip()):
                count_atom_forces = atom_count
            if re.search(regex_match_total_stress, line):
                count_total_stress = 3
            if re.search(regex_end, line):
                configs.append(copy.deepcopy(config))
                config = Config(0, [], [], [], [])
    new_config_f = open("test.cfg", "w")

    for config in configs:
        new_config_f.write("BEGIN_CFG\n")
        new_config_f.write(" Size\n")
        new_config_f.write("    " + str(atom_count) + "\n")
        new_config_f.write(" Supercell\n")

        for cell in config.cell_parametrs:
            new_config_f.write("         " + '      '.join(str(e) for e in cell) + "\n")
        new_config_f.write(
            " AtomData:  id type       cartes_x      cartes_y      cartes_z           fx          fy          fz\n")
        i = 1
        atom_forces = config.atom_forces
        for pos in config.atom_pos:
            force = atom_forces[i - 1]
            new_config_f.write("            " + str(i) + "    0       " + "{:10.6f}".format(pos[0]) + "      " +
                               "{:10.6f}".format(pos[1]) + "      " + "{:10.6f}".format(pos[2]) + "      " +
                               "{:10.6f}".format(force[0]) + "      " + "{:10.6f}".format(force[1]) + "      " +
                               "{:10.6f}".format(force[2]) + "\n")
            i = i + 1
        new_config_f.write(" Energy\n")
        new_config_f.write("       " + str(config.energy) + "\n")
        new_config_f.write(" PlusStress:  xx          yy          zz          yz          xz          xy\n")
        cells = config.total_stress
        new_config_f.write(
            "              " + "{:10.6f}".format(cells[0][0]) + "          " +
            "{:10.6f}".format(cells[1][1]) + "          " + "{:10.6f}".format(cells[2][2]) + "          " +
            "{:10.6f}".format(cells[1][2]) + "          " + "{:10.6f}".format(cells[0][2]) + "          " +
            "{:10.6f}".format(cells[0][1]) + "\n")
        new_config_f.write(" Feature   EFS_by	VASP\n")
        new_config_f.write("END_CFG\n\n")


test()
